// Objet dessin SVG
// Prend en argument l'identifiant css d'un node svg présent dans le dom
function DrawSVG(node) {

	// Id du noeud contenant le formulaire de dessin
	this.node = node;
    this.id_node = '#'+node;

	// Charge un objet SVG fournis par SVG.js
	this.svg = SVG(this.id_node + '_svg');

	// coordonnées de l'objet
	this.x = $(this.id_node + '_svg').offset().left;
	this.y = $(this.id_node + '_svg').offset().top;
	this.w = $(this.id_node + '_svg').width();
	this.h = $(this.id_node + '_svg').height();

	// Pile utilisée pour annuler et refaire des actions de dessin
	this.undostack = [];
	this.redostack = [];

	// Outil de dessin sélectionné
	this.drawtool = '';

	// Flag indiquant si un dessin est en cours ou non
	this.drawing = false;

    // Noeud actuellement sélectionné
    this.cur_selected = {id: '', runner: undefined};

    // class pour les boutons sélectionnés et non sélectionnés
    this.class_button = {selected: 'btn-secondary', nonselected: 'btn-outline-secondary'};

    // Description actuellement sélectionné pour suppression
    this.desc_to_rm = '';

    // Description sélectionné
    this.desc_selected = "0";

    // Couleur du trait dessiné
    this.stroke_color = "#000000";

    // Taille du trait dessiné
    this.stroke_width = "6";

    // Groupe de description sélectionné
    this.cur_group = '';

	/*
	 * Définition des méthodes
	 */

	// Sélection de l'outil de dessin
	this.selectTool = function(tool) {
	    // check if selected tool is defined
        if (typeof eval('DrawTool_'+tool) !== 'function') {
            throw 'Unrefined tool';
        }
	    if (typeof this.drawtool.name !== 'undefined' && this.drawtool.name !== '') {
            $(this.id_node +'_tool-' + this.drawtool.name).removeClass(this.class_button.selected);
            $(this.id_node +'_tool-' + this.drawtool.name).addClass(this.class_button.nonselected);
	    };
        this.drawtool = eval('new DrawTool_'+tool+'(this)');
	    $(this.id_node +'_tool-' + tool).removeClass(this.class_button.nonselected);
	    $(this.id_node +'_tool-' + tool).addClass(this.class_button.selected);
        // Valeur actuelle de taille du crayon
        this.stroke_width = $(this.id_node + '_tools_param_width')[0].value;
	}

    // Sélection d'une description
    this.selectDesc = function(desc_num) {
        // Marque comme non sélectionné l'ancienne entrée 
        $(this.id_node+'_desc-'+this.desc_selected).removeClass('active');
        $(this.id_node+'_desc-'+this.desc_selected+'-select').removeClass('btn-primary');
        $(this.id_node+'_desc-'+this.desc_selected+'-select').addClass('btn-outline-primary');
        $(this.id_node+'_desc-'+this.desc_selected+'-select').removeAttr('disabled');
        // Marque comme sélectionne la nouvelle entrée
        $(this.id_node+'_desc-'+desc_num).addClass('active');
        $(this.id_node+'_desc-'+desc_num+'-select').removeClass('btn-outline-primary');
        $(this.id_node+'_desc-'+desc_num+'-select').addClass('btn-primary');
        $(this.id_node+'_desc-'+desc_num+'-select').attr('disabled', 'disabled');
        // Marque la nouvelle sélection
        this.desc_selected = desc_num;
        // Change la couleur de dessin avec la couleur de la sélection
        this.selectGroup(desc_num);
    }

    // Définis un groupe SVG en fonction de la description sélectionnée
    this.selectGroup = function(desc_num) {
        this.cur_group = SVG(this.id_node+'_shape_group-'+desc_num);
        // Si le groupe n'existe pas le crée
        if (this.cur_group === null) {
            this.cur_group = this.svg.group().attr({
                'id': this.node+'_shape_group-'+desc_num,
                'fill': 'none',
                'stroke': $(this.id_node+'_desc-'+desc_num+'-color')[0].value,
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round' 
            });
        }
    }

    // Ajout la dernière action à la pile undo
    this.addToUndo = function(elemid) {
        this.undostack.push({
            id: elemid,
            tool: this.drawtool.name,
            shape: $('#'+elemid)[0].outerHTML,
            group: $('#'+elemid)[0].parentNode.id
        });
    }

    // Stop une action de dessin
    this.stopDrawing = function() {
        this.addToUndo(this.drawtool.endDraw());
        this.drawing = false;
        // Purge de la pile des actions à refaire
        if (this.redostack.length > 0) { this.redostack = []; }
    }

    // Stop une sélection de form
    this.stopShapeSelect = function() {
        let cur_id = this.cur_selected.id;
        let base_color = this.cur_selected.base_color;
        SVG('#' + cur_id).timeline().stop();
        this.cur_selected.runner.unschedule();
        this.cur_selected = {
            id: '',
            color: '#000000',
            runner: undefined
        }
        $('#' + cur_id).removeAttr('stroke');
    }

	/*
	 * Utilisation du système d'évènement de SVG.js pour gérer
	 * les actions de dessin
	 */

    // Quand le doigt est appuyé commence le dessin
	this.svg.on('touchstart', function(e) {
        if (e.buttons == 1) {
            // Cas ou une action de dessin serait déjà en cours
            // (peut arriver sur une double tap rapide)
            if (this.drawing) {
                this.stopDrawing();
            }
            // Si l'outil actuellement sélectionné est la gomme et qu'elle
            // survole une forme
            if (this.drawtool.name == 'rubber') {
                if (this.cur_selected.id === e.target.id) {
                    this.stopShapeSelect();
                    this.addToUndo(e.target.id);
                    $('#'+e.target.id).remove();
                }
            // Pour les autres outils
            } else {
                this.drawing = true;
                this.drawtool.beginDraw(e.layerX, e.layerY);
            }
        }
	}, this);

	// Doigt se déplaçant  sur le dessin
    this.svg.on('touchmove', function(e) {
        //console.log(this.cur_selected);
        if (this.drawing) {
            if (e.buttons == 1) {
                this.drawtool.doDraw(e.layerX, e.layerY);
            } else {
                this.stopDrawing();
            }
        } else {
            if (e.target.id !== this.node+'_svg' && this.cur_selected.id === '') {
                // Marque la forme comme sélectionnée au survol de la souris
                // avec l'outil gomme
                if (this.drawtool.name === 'rubber') {
                    this.cur_selected = {
                        id: e.target.id,
                        base_color: SVG('#' + e.target.id).attr('stroke'),
                        runner: this.cur_selected.runner = SVG('#'+e.target.id).animate({'duration': 200, 'delay': 0, 'when': 'now'}).stroke({ color: '#f00' }).animate({'duration': 200, 'delay': 200, 'when': 'now'}).stroke({ color: '#ff0' }).loop(true, true)
                    }
                }
            } else if (e.target.id !== this.cur_selected.id) {
                // Quand une sélection existe si une autre forme est survolée
                // stop la sélection
                if (this.cur_selected.id != '') {
                    this.stopShapeSelect();
                }
            }
        }
	}, this);

	// Quand la souris est pressée commence le dessin
	this.svg.on('mousedown', function(e) {
        if (e.buttons == 1) {
            // Cas ou un action de dessin serait déjà en cours
            // (peut arriver sur un double clique rapide)
            if (this.drawing) {
                this.stopDrawing();
            }
            // Si l'outil actuellement sélectionné est la gomme et qu'elle
            // survole une forme
            if (this.drawtool.name == 'rubber') {
                if (this.cur_selected.id === e.target.id) {
                    this.stopShapeSelect();
                    this.addToUndo(e.target.id);
                    $('#'+e.target.id).remove();
                }
            // Pour les autres outils
            } else {
                this.drawing = true;
                this.drawtool.beginDraw(e.layerX, e.layerY);
            }
        }
	}, this);

	// Souris se déplaçant  sur le dessin
    this.svg.on('mousemove', function(e) {
        //console.log(this.cur_selected);
        if (this.drawing) {
            if (e.buttons == 1) {
                this.drawtool.doDraw(e.layerX, e.layerY);
            } else {
                this.stopDrawing();
            }
        } else {
            if (e.target.id !== this.node+'_svg' && this.cur_selected.id === '') {
                // Marque la forme comme sélectionnée au survol de la souris
                // avec l'outil gomme
                if (this.drawtool.name === 'rubber') {
                    this.cur_selected = {
                        id: e.target.id,
                        base_color: SVG('#' + e.target.id).attr('stroke'),
                        runner: this.cur_selected.runner = SVG('#'+e.target.id).animate({'duration': 200, 'delay': 0, 'when': 'now'}).stroke({ color: '#f00' }).animate({'duration': 200, 'delay': 200, 'when': 'now'}).stroke({ color: '#ff0' }).loop(true, true)
                    }
                }
            } else if (e.target.id !== this.cur_selected.id) {
                // Quand une sélection existe si une autre forme est survolée
                // stop la sélection
                if (this.cur_selected.id != '') {
                    this.stopShapeSelect();
                }
            }
        }
	}, this);

	/*
	 * Initialisation de l'objet
	 */

	// Écouteur pour détecter le changement d'outil
	$(this.id_node + '_tools button').on('click', {obj: this}, function(e) {
	    let tool = this.getAttribute('data-tool');
	    if (typeof e.data.obj.drawtool.name !== 'undefined' || e.data.obj.drawtool.name !== '') {
            e.data.obj.selectTool(tool);
	    }
	});

    // Écouteur pour le changement de couleur
    $(this.id_node + '_tools_param_color').on('change', {obj: this}, function(e) {
        e.data.obj.drawtool.strokeColor = e.target.value;
    });

    // Écouteur pour le changement de la taille du crayon
    $(this.id_node + '_tools_param_width').on('change', {obj: this}, function(e) {
        e.data.obj.stroke_width = e.target.value;
    });

	// Capte le clique sur le bouton Annuler
	$(this.id_node + '_btn-undo').on('click', {obj: this}, function(e) {
        if (e.data.obj.undostack.length > 0) {
            let last_action = e.data.obj.undostack.pop();
            // Si la dernière action est effectuée avec la gomme
            // Recrée la forme sinon la supprime
            if (last_action.tool === 'rubber') {
                SVG('#'+last_action.group).add(last_action.shape);
            } else {
                $('#'+last_action.id).remove();
            }
            e.data.obj.redostack.push(last_action);
        }
	});

	// Capte le clique sur le bouton refaire
	$(this.id_node + '_btn-redo').on('click', {obj: this}, function(e) {
        if (e.data.obj.redostack.length > 0) {
            let last_undo = e.data.obj.redostack.pop();
            if (last_undo.tool === 'rubber') {
                $('#'+last_undo.id).remove();
            } else {
                SVG('#'+last_undo.group).add(last_undo.shape);
            }
            e.data.obj.undostack.push(last_undo);
        }
	});

    /*
     * Gestion des descriptions
     */

    // Capte le clique sur le bouton d'ajout de description
    $(this.id_node + '_add_desc').on('click', {obj: this}, function(e) {
        // Récupére le numéro de la dernière desc et l'incrémente de 1
        new_desc_num = parseInt($('.' + e.data.obj.node + '_desc').last()[0].dataset.desc_num) + 1;
        // Ajoute la description
        $(e.data.obj.id_node + '_desc-group').append('\
            <li id="'+ e.data.obj.node +'_desc-'+ new_desc_num +'" class="list-group-item list-group-item-action '+e.data.obj.node+'_desc" data-desc_num="'+new_desc_num+'">\
                <div class="input-group">\
                    <div class="input-group-prepend">\
                        <button id="mydrawfield_desc-'+new_desc_num+'-select" class="form-control btn btn-outline-primary '+e.data.obj.node+'_desc-select" type="button" title="Séléctioner la déscription" data-desc_num="'+new_desc_num+'">&#10004;</button>\
                        <input id="mydrawfield_desc-'+new_desc_num+'-color" type="color" class="form-control input-group-text" style="width: 8em" data-desc_num="'+new_desc_num+'" value="#'+Math.random().toString(16).slice(-6)+'">\
                    </div>\
                    <input class="form-control" type="text" name="mydrawfiled_desc-text['+new_desc_num+']" id="mydrawfiled_desc-'+new_desc_num+'-entry" value="">\
                    <div class="input-group-append">\
                        <button class="form-control btn btn-outline-danger '+e.data.obj.node+'_desc-rm" type="button" data-desc_num="'+new_desc_num+'" title="Supprimer la déscription">&times;</button>\
                    </div>\
                </div>\
            </li>\
        ');
    });


    // Capte les clique sur les boutons de sélection ou de suppression
    // d'une sélection
    $(this.id_node + '_desc-group').on('click', 'button', {obj: this}, function(e) {
        // Bouton sélection
        if (e.target.classList.contains(e.data.obj.node+'_desc-select')) {
            // ne rien faire si l'entrée est déjà séléctionnée
            if (e.target.dataset.desc_num == e.data.obj.desc_selected) { return }
            e.data.obj.selectDesc(e.target.dataset.desc_num);
        }
        // Bouton suppression
        else if (e.target.classList.contains(e.data.obj.node+'_desc-rm')) {
                let desc_num = e.target.dataset.desc_num;
                e.data.obj.desc_to_rm = desc_num;
                $(e.data.obj.id_node+'_desc-rm_dialg').modal('toggle');
        }
    });

    // Détecte les changements de couleur des définitions
    $(this.id_node + '_desc-group').on('change', 'input[type=color]', {obj: this}, function(e) {
        let desc_num = e.target.dataset.desc_num;
        let group = SVG(e.data.obj.id_node+'_shape_group-'+desc_num);
        if (group != null) {
            group.stroke(e.target.value);
        }
    });

    // Clique sur le bouton supprimer dans la modal de confirmation de
    // suppression de description
    $(this.id_node+'_desc-rm_dialg-confirm').on('click', {obj: this}, function(e) {
        let current_desc_num = e.data.obj.desc_to_rm;
        $(e.data.obj.id_node+'_desc-'+current_desc_num).remove();
        $(e.data.obj.id_node+'_shape_group-'+current_desc_num).remove();
        // Cas ou la description supprimée serait actuellement sélectionné
        if (current_desc_num == e.data.obj.desc_selected) {
            e.data.obj.selectDesc(0); // sélection de la description de base
        }
    });

    // Quand la modal de suppression de la description se ferme supprime
    // l'id de la description à supprimer
    $(this.id_node+'_desc-rm_dialg').on('hidden.bs.modal', {obj: this}, function(e) {
        e.data.obj.desc_to_rm = '';
    });

	// Outil sélectionné par défaut
	this.selectTool('square');

    this.selectGroup(this.desc_selected);

    /*
     * Définition des outils de dessin
     */

     // Prototype d'outil
     function DrawTool(sheet, name) {

        // Mémorise la feuille de dessin
        this.sheet = sheet;

        // Nom de la forme
        this.name = name;

        // Forme couramment dessiné
        this.cur_shape = undefined;

        // Position initale au début du dessin
        this.begin_pos = {'x': 0, 'y': 0};

        // Génére un id aléatoire pour la forme en cours de dessin
        this.genRandomId = function() {
            let rand_num = undefined;
            let rand_id = undefined;
            do {
                rand_num = Math.floor((Math.random() * 1000000) + 1);
                rand_id = this.sheet.node + '_shape-' + rand_num;
            } while ($(rand_id).length > 0) // évite les doublons
            return rand_id;
        }

        // Calcul la position relative du curseur de la souris
        this.calcCurPos = function(x, y) {
            let curpos = {
                'x': x - this.sheet.x,
                'y': y - this.sheet.y 
            };
            return curpos;
        }

        // Méthode appelée au début d'un dessin
        this.beginDraw = function (e_pos_x, e_pos_y) {};

        // Méthode durant le dessin
        this.doDraw = function() {};

        // Méthode appelée en fin de dessin
        this.endDraw = function() {
            let shape_id = this.genRandomId();
            this.cur_shape.attr({
                'id': shape_id,
                'class': this.sheet.node + '_shape'
            });
            return shape_id;
        };
    }

    // dessin d'un carré
    function DrawTool_square(sheet) {

        // Héritage de propriété et méthode de DrawAction
        DrawTool.call(this, sheet, 'square');

        this.beginDraw = function (e_pos_x, e_pos_y) {
            this.begin_pos = this.calcCurPos(e_pos_x, e_pos_y);
            this.cur_shape = this.sheet.cur_group.rect(1, 1);
            this.cur_shape.move(this.begin_pos.x, this.begin_pos.y);
        };

        this.doDraw = function(e_pos_x, e_pos_y) {
            this.cur_pos = this.calcCurPos(e_pos_x, e_pos_y);
            this.cur_shape.stroke({'width': this.sheet.stroke_width});

            // 4 cas de figure en fonction de la position du curseur
            // par rapport à l'origine

            // - bas droite
            if (this.cur_pos.x > this.begin_pos.x && this.cur_pos.y > this.begin_pos.y) {
                this.cur_shape.attr({
                    'width': this.cur_pos.x - this.begin_pos.x,
                    'height': this.cur_pos.y - this.begin_pos.y
                });

            // - bas gauche
            } else if (this.cur_pos.x < this.begin_pos.x && this.cur_pos.y > this.begin_pos.y) {
                this.cur_shape.attr({
                    'x': this.cur_pos.x,
                    'width': this.begin_pos.x - this.cur_pos.x,
                    'height': this.cur_pos.y - this.begin_pos.y
                });

            // - haut droite
            } else if (this.cur_pos.x > this.begin_pos.x && this.cur_pos.y < this.begin_pos.y) {
                this.cur_shape.attr({
                    'y': this.cur_pos.y,
                    'width': this.cur_pos.x - this.begin_pos.x,
                    'height': this.begin_pos.y - this.cur_pos.y
                });

            // - haut gauche
            } else if (this.cur_pos.x < this.begin_pos.x && this.cur_pos.y < this.begin_pos.y) {
                this.cur_shape.attr({
                    'y': this.cur_pos.y,
                    'x': this.cur_pos.x,
                    'width': this.begin_pos.x - this.cur_pos.x,
                    'height': this.begin_pos.y - this.cur_pos.y
                });
            }
        }
        
    }

    // dessin d'un cercle
    function DrawTool_circle(sheet) {

        // Héritage de propriété et méthode de DrawAction
        DrawTool.call(this, sheet, 'circle');

        this.beginDraw = function (e_pos_x, e_pos_y) {
            this.begin_pos = this.calcCurPos(e_pos_x, e_pos_y);
            this.cur_shape = this.sheet.cur_group.ellipse(1, 1);
            this.cur_shape.move(this.begin_pos.x, this.begin_pos.y);
        };

        this.doDraw = function(e_pos_x, e_pos_y) {
            this.cur_pos = this.calcCurPos(e_pos_x, e_pos_y);
            this.cur_shape.stroke({'width': this.sheet.stroke_width});

            // 4 cas de figure en fonction de la position du curseur
            // par rapport à l'origine

            // - bas droite
            if (this.cur_pos.x > this.begin_pos.x && this.cur_pos.y > this.begin_pos.y) {
                let half_x = (this.cur_pos.x - this.begin_pos.x) / 2;
                let half_y = (this.cur_pos.y -this.begin_pos.y) / 2;
                this.cur_shape.attr({
                    'cx': this.begin_pos.x + half_x,
                    'cy': this.begin_pos.y + half_y,
                    'rx': half_x,
                    'ry': half_y
                });

            // - bas gauche
            } else if (this.cur_pos.x < this.begin_pos.x && this.cur_pos.y > this.begin_pos.y) {
                let half_x = (this.begin_pos.x - this.cur_pos.x) / 2;
                let half_y = (this.cur_pos.y - this.begin_pos.y) / 2;
                this.cur_shape.attr({
                    'cx': this.cur_pos.x + half_x,
                    'cy': this.begin_pos.y + half_y,
                    'rx': half_x,
                    'ry': half_y
                });

            // - haut droite
            } else if (this.cur_pos.x > this.begin_pos.x && this.cur_pos.y < this.begin_pos.y) {
                let half_x = (this.cur_pos.x - this.begin_pos.x) / 2;
                let half_y = (this.begin_pos.y - this.cur_pos.y) / 2;
                this.cur_shape.attr({
                    'cx': this.begin_pos.x + half_x,
                    'cy': this.cur_pos.y + half_y,
                    'rx': half_x,
                    'ry': half_y
                });

            // - haut gauche
            } else if (this.cur_pos.x < this.begin_pos.x && this.cur_pos.y < this.begin_pos.y) {
                let half_x = (this.begin_pos.x - this.cur_pos.x) / 2;
                let half_y = (this.begin_pos.y - this.cur_pos.y) / 2;
                this.cur_shape.attr({
                    'cx': this.cur_pos.x + half_x,
                    'cy': this.cur_pos.y + half_y,
                    'rx': half_x,
                    'ry': half_y
                });
            }
        }
    }

    // dessin au crayon
    function DrawTool_pen(sheet) {

        // Héritage de propriété et méthode de DrawAction
        DrawTool.call(this, sheet, 'pen');

        // Tableau de point constituant la ligne
        this.points = '';

        this.beginDraw = function (e_pos_x, e_pos_y) {
            let cur_pos = this.calcCurPos(e_pos_x, e_pos_y);
            this.points += cur_pos.x+','+cur_pos.y;
            this.cur_shape = this.sheet.cur_group.polyline(this.points);
        };

        this.doDraw = function(e_pos_x, e_pos_y) {
            let cur_pos = this.calcCurPos(e_pos_x, e_pos_y);
            this.points += ' '+cur_pos.x+','+cur_pos.y;
            this.cur_shape.attr({points: this.points});
            this.cur_shape.stroke({'width': this.sheet.stroke_width});
        }

        this.endDraw = function() {
            this.points = '';
            let shape_id = this.genRandomId();
            this.cur_shape.attr({
                'id': shape_id,
                'class': this.sheet.node + '_shape'
            });
            return shape_id;
        };
    }

    // Gomme (comportement spécial différent des autres outils)
    function DrawTool_rubber(sheet) {

        // Mémorise la feuille de dessin
        this.sheet = sheet;

        // Nom de la forme
        this.name = 'rubber';

        // Méthode appelée au début d'un dessin
        this.beginDraw = function (e_pos_x, e_pos_y) {};

        // Méthode durant le dessin
        this.doDraw = function() {};

        // Méthode appelée en fin de dessin
        this.endDraw = function() {};
    }

};
// set sw=4 sts=4 ts=4 expandtab
